import os
import numpy as np
import torch
from agg_loss import CasRel_AggLoss
from agg_metric import AggMetric
from data_loader import *
from model import CasRelModel
from torch.optim import Adam
from fastNLP import Tester, Vocabulary, AccuracyMetric,Trainer
from fastNLP.embeddings import BertEmbedding
from fastNLP.io import DataBundle
from transformers import AutoConfig, AutoModel, AutoTokenizer

data_folder = './data/NYT'
train_path = os.path.join(data_folder, 'raw_train.json')
dev_path = os.path.join(data_folder, 'raw_valid.json')
test_path = os.path.join(data_folder, 'raw_test.json')

bert_model = 'bert-base-cased'
tokenizer = AutoTokenizer.from_pretrained(bert_model)
config = AutoConfig.from_pretrained(bert_model)
embed = AutoModel.from_pretrained(bert_model, config=config)


train_dataset, relation_ids = generate_dataset(train_path, tokenizer)
dev_dataset, _ = generate_dataset(dev_path,tokenizer, relation_ids)
src_vocab = Vocabulary()
train_dataset.add_seq_len('words')
dev_dataset.add_seq_len('words')

data_bundle = DataBundle(datasets={"train": train_dataset, "dev": dev_dataset})

device = 0 if torch.cuda.is_available() else 'cpu'  # 如果有gpu的话在gpu上运行，训练速度会更快

def fn_generate_pair(ins_list):
    x = []

    sentens_len = []
    for ind, ins in ins_list:
        sentens_len.append(ins['seq_len'])
    max_length = max(sentens_len)

    subject_start_target = torch.zeros((len(ins_list), max_length))
    subject_end_target = torch.zeros((len(ins_list), max_length))
    selected_subs = []
    selected_sub_indexs = []
    for index, (ind, ins) in enumerate(ins_list):
        # [head,tail]
        subject_start_target[index, ins['subjects'][:, 0]] = 1
        subject_end_target[index, ins['subjects'][:, 1]] = 1
        selected_sub_index = np.random.randint(len(ins['subjects']))
        selected_sub = ins['subjects'][selected_sub_index, :]
        selected_subs.append(selected_sub)
        selected_sub_indexs.append(selected_sub_index)

    obj_start_target = torch.zeros((len(ins_list), len(relation_ids), max_length))
    obj_end_target = torch.zeros_like(obj_start_target)

    for index, selected_sub_index in enumerate(selected_sub_indexs):
        selected_objects = ins_list[index][1]['objects'][selected_sub_index]
        for obj in selected_objects:
            obj_head, obj_tail, relation = obj[0], obj[1], obj[2]
            obj_start_target[index, relation, obj_head] = 1
            obj_end_target[index, relation, obj_tail] = 1
    selected_subs = np.array(selected_subs)  ##torch.tensor(selected_subs)
    return {'selected_subs': selected_subs}, \
           {'subject_start_target': subject_start_target, 'subject_end_target': subject_end_target,
            'obj_start_target': obj_start_target, 'obj_end_target': obj_end_target}


data_bundle.add_collate_fn(fn_generate_pair)
data_bundle.set_ignore_type('subjects', 'objects')
data_bundle.set_input('words', 'selected_subs')
data_bundle.set_target('subject_start_target', 'subject_end_target', 'obj_start_target', 'obj_end_target', 'subjects',
                       'objects','seq_len')
data_bundle.set_vocab(src_vocab, 'words')
word_vocab = data_bundle.get_vocab('words')

model = CasRelModel(embed, label_size=len(relation_ids))

loss = CasRel_AggLoss(len(relation_ids))
optimizer = Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=1e-5)

metric = AggMetric() #AccuracyMetric(pred='sub_start_pred', target='subject_start_target')
device = 0 if torch.cuda.is_available() else 'cpu'  # 如果有gpu的话在gpu上运行，训练速度会更快

trainer = Trainer(data_bundle.get_dataset('train'), model=model, loss=loss,print_every=100,
                  optimizer=optimizer, batch_size=8, dev_data=data_bundle.get_dataset('dev'), device=device,metrics=metric, use_tqdm=False)
trainer.train()  

# tester = Tester(data_bundle.get_dataset('dev'), model, metrics=AggMetric(), use_tqdm=False)
# tester.test()
