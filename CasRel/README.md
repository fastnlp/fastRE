## A Novel Cascade Binary Tagging Framework for Relational Triple Extraction

### 论文

https://arxiv.org/abs/1909.03227

### 使用数据集：

[NYT](https://drive.google.com/file/d/1kAVwR051gjfKn3p6oKc7CzNT9g2Cjy6N/view)

### 训练过程：

1. 安装fastnlp和huggingface transformers
2. 下载数据集，解压缩到./data/NYT目录
3. 使用python train.py 或者train.ipynb 进行训练

### 结果：

论文在NYT数据集的F1值为 0.896

我们的复现结果

F1=0.7890351542218025, Precision=0.8799691698001668, Recall=0.715216021099361

在原论文的官方实现中，发现他的数据处理是存在问题的，导致识别实体，被缩减成单个词语。降低了识别难度。
https://github.com/weizhepei/CasRel/issues/49
https://github.com/weizhepei/CasRel/issues/47





