from fastNLP.core.metrics import MetricBase


class AggMetric(MetricBase):
    def __init__(self):
        super().__init__()
        self.truth = 0  # 样本阳性
        self.true_positive = 0  # 真阳性
        self.pred_positive = 0  # 预测的阳性

    def evaluate(self, triples, subjects, objects):
        for batch_index, batch_triple in enumerate(triples):
            row_subjects = subjects[batch_index]
            row_object = objects[batch_index]
            ground_truth = set()
            predict_triple = set()
            for sub_index, sub in enumerate(row_subjects):
                sub_objects = row_object[sub_index]
                for obj in sub_objects:
                    ground_truth.add('-'.join([str(t) for t in [sub[0],sub[1],obj[0], obj[1], obj[2]]]))
            for triple in batch_triple:
                predict_triple.add('-'.join(str(c) for c in triple))
            self.truth += len(ground_truth)
            self.true_positive += len(ground_truth & predict_triple)
            self.pred_positive += len(predict_triple)

    def get_metric(self, reset=True):  # 在这里定义如何计算metric
        print(f'true_positive:{self.true_positive},pred_positive:{self.pred_positive},truth:{self.truth}')

        precise = self.true_positive / (self.pred_positive + 0.0001)
        recall = self.true_positive / (self.truth+ 0.0001)
        f1 = 2 * precise * recall / (precise + recall + + 0.0001)
        if reset:  # 是否清零以便重新计算
            self.true_positive = 0
            self.truth = 0
            self.pred_positive = 0

        return {'f1': f1, 'precise': precise,
                'recall': recall}  # 需要返回一个dict，key为该metric的名称，该名称会显示到Trainer的progress bar中
