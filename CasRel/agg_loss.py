import numpy as np
import torch
from torch.functional import F
from torch.nn import BCELoss
from fastNLP import LossBase


class CasRel_AggLoss(LossBase):
    def __init__(self, label_size):
        super(CasRel_AggLoss, self).__init__()
        #self._init_param_map(pred=pred)
        self.label_size = label_size
        self.bce_loss = torch.nn.BCELoss()

    def get_loss(self, sub_start_pred, sub_end_pred, obj_start_pred, obj_end_pred,
                 subject_start_target, subject_end_target, obj_start_target, obj_end_target):
       
        sub_start_loss = self.bce_loss(sub_start_pred,subject_start_target).sum()
        sub_end_loss = self.bce_loss(sub_end_pred, subject_end_target).sum()
        obj_start_loss = self.bce_loss(obj_start_pred, obj_start_target).sum()
        obj_end_loss = self.bce_loss(obj_end_pred, obj_end_target).sum()
        return sub_start_loss+sub_end_loss+obj_start_loss+obj_end_loss
