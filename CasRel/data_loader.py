import json
import numpy as np
import unicodedata
from fastNLP import DataSet, Instance


def load_json_from_file(path):
    with open(path) as f:
        return json.load(f)


# def tokenize(sentence):
#     # 英文字符直接空格切分。
#     # fastnlp内部会使用bert的tokenizer，但word piece 会使用firt，last，avg等方式合并。所以词序无变化。
#     return sentence.split()


def find_head_tail(sentence, entity):
    entity_len = len(entity)
    sent_len = len(sentence)
    for i in range(sent_len - entity_len + 1):
        if sentence[i:i + entity_len] == entity:
            return i, i + entity_len - 1
    return -1, -1

def remove_specical_char(sentence):
    text = unicodedata.normalize('NFD', sentence)
    text = ''.join([ch for ch in text if unicodedata.category(ch) != 'Mn'])
    text = text.lower()
    spaced = ''
    for ch in text:
        if ord(ch) == 0 or ord(ch) == 0xfffd or (unicodedata.category(ch) in ('Cc', 'Cf')):
            continue
        else:
            spaced += ch
    return spaced


def generate_dataset(file_path, tokenzier, relation_ids_mapping=None):
    relation_ids=set()
    data_rows = []
    dataset = DataSet()
    with open(file_path) as f:
        for line in f:
            sample = json.loads(line)
            raw_text = remove_specical_char(sample['sentText'])
            raw_sent = tokenzier.tokenize(raw_text)
            raw_relations = sample['relationMentions']
            subjects = {}
            objects = {}
            for relation in raw_relations:
                if relation['label'] == 'None':
                    print('found label is none')
                    continue
                em1Text = remove_specical_char(relation['em1Text'])
                em2Text = remove_specical_char(relation['em2Text'])
                subject_head, subject_tail = find_head_tail(raw_sent, tokenzier.tokenize(em1Text))#[1:-1]
                object_head, object_tail = find_head_tail(raw_sent, tokenzier.tokenize(em2Text))#[1:-1]
                if object_head == -1:
                    continue
                if subject_head == -1:
                    print('can not find subject: ', tokenzier.tokenize(em1Text))#[1:-1]
                    continue
                if em1Text not in subjects:
                    subjects[em1Text] = [subject_head, subject_tail]
                if em1Text not in objects:
                    objects[em1Text] = []
                objects[em1Text].append([object_head, object_tail, relation['label']])
                relation_ids.add(relation['label'])
            data_rows.append([raw_text, subjects, objects, ])
    if relation_ids_mapping is None:
        relation_ids_mapping = {relation: index for index, relation in enumerate(relation_ids)}
    # 转化成数字
    for row in data_rows:
        sub_id_mapping = {sub_key: index for index, sub_key in enumerate(row[1].keys())}
        subject_data = [[] for i in range(len(sub_id_mapping.keys()))]
        object_data = [[] for i in range(len(sub_id_mapping.keys()))]
        for key in row[1].keys():
            subject_data[sub_id_mapping[key]] = row[1][key]  # [[1,3],[3,4]]
        for key in row[2].keys():
            objects = row[2][key]
            for obj_index in range(len(objects)):
                objects[obj_index][2] = relation_ids_mapping[objects[obj_index][2]]  # 关系类型 编码
            object_data[sub_id_mapping[key]] = objects
        # subjects (sub_size,2)
        # objects (sub_size, object_size,3)
        if(len(subject_data) == 0):
            print('found empty subject')
            print(sub_id_mapping)
        subject_data = np.array(subject_data)
        #object_data = np.array(object_data)

        token_ids  = tokenzier.encode(text=row[0],add_special_tokens=False)#segment_ids

        dataset.append(Instance(words=token_ids, seq_len=len(token_ids),
                                subjects=subject_data, objects=object_data))
    return dataset,relation_ids_mapping
