import numpy as np
import torch
import torch.nn as nn


class CasRelModel(nn.Module):
    def __init__(self, embed, label_size=32, dim=768):
        super(CasRelModel, self).__init__()
        self.embed = embed
        self.activation = nn.Sigmoid()
        self.sub_start_layer = nn.Linear(dim, 1)
        self.sub_end_layer = nn.Linear(dim, 1)
        self.obj_start_layers = []
        self.obj_end_layers = []
        self.label_size = label_size
        self.obj_start_layers = nn.ModuleList([nn.Linear(dim, 1) for i in range(label_size)])
        self.obj_end_layers = nn.ModuleList([nn.Linear(dim, 1) for i in range(label_size)])
        self._threshold = 0.5

    @property
    def threshold(self):
        return self._threshold
    
    @threshold.setter
    def threshold(self, value):
        self._threshold = value
        
    def forward(self, words, selected_subs):
        obj_start_pred = []
        obj_end_pred = []
        # 预测
        x_emb = self.embed(input_ids=words)[0]
        # [batch, seq_len]
        sub_start_pred = self.activation(self.sub_start_layer(x_emb)).squeeze(dim=-1)
        sub_end_pred = self.activation(self.sub_end_layer(x_emb)).squeeze(dim=-1)

        for batch_index in range(words.shape[0]):
            one_sent_embed = x_emb[batch_index]
            #sub_v = one_sent_embed[selected_subs[batch_index][1], :]
            #sub_embed =  one_sent_embed[selected_subs[batch_index][0]: selected_subs[batch_index][1]+1, :]
            #sub_v = torch.mean(sub_embed, dim=0, keepdim=True)
            sub_v = (one_sent_embed[selected_subs[batch_index][0]] + one_sent_embed[selected_subs[batch_index][1]])/2
            object_embed = sub_v + one_sent_embed
            obj_relation_start_pred = []
            obj_relation_end_pred = []
            for i in range(self.label_size):
                obj_relation_start_pred.append(self.activation(self.obj_start_layers[i](object_embed)).squeeze(dim=-1))
                obj_relation_end_pred.append(self.activation(self.obj_end_layers[i](object_embed)).squeeze(dim=-1))
            obj_relation_start_pred = torch.stack(obj_relation_start_pred, dim=0)
            obj_relation_end_pred = torch.stack(obj_relation_end_pred, dim=0)
            obj_start_pred.append(obj_relation_start_pred)
            obj_end_pred.append(obj_relation_end_pred)
        obj_start_pred = torch.stack(obj_start_pred, dim=0)
        obj_end_pred = torch.stack(obj_end_pred, dim=0)
        return {'obj_start_pred': obj_start_pred, 'obj_end_pred': obj_end_pred, 'sub_start_pred': sub_start_pred,
                'sub_end_pred': sub_end_pred}
    
    
    def _find_near_ht(self, head_pred, tail_pred):
        """
        查最相邻的head和tail
        """
        heads_index = np.where(head_pred > self._threshold)[0]
        tails_index = np.where(tail_pred > self._threshold)[0]
        result = []
        for head in heads_index:
            found_tail = None
            for tail in tails_index:
                if tail >= head:
                    found_tail = tail
                    break
            if found_tail:
                result.append([head, found_tail])
        return result

    def predict(self, words):
        """
        预测可能的三元组。
        """
        found_triple = []
        with torch.no_grad():
            x_emb = self.embed(input_ids=words)[0]
            # [batch, seq_len]
            sub_start_pred = self.activation(self.sub_start_layer(x_emb)).squeeze(dim=-1).cpu().numpy()
            sub_end_pred = self.activation(self.sub_end_layer(x_emb)).squeeze(dim=-1).cpu().numpy()
            for batch_index in range(sub_start_pred.shape[0]):
                found_triple.append([])
                one_sent_embed = x_emb[batch_index]
                found_subs = self._find_near_ht(sub_start_pred[batch_index], sub_end_pred[batch_index])
                for found_sub in found_subs:
                    #sub_embed = one_sent_embed[found_sub[0]: found_sub[1]+1 , :]
                    sub_v = (one_sent_embed[found_sub[0]] + one_sent_embed[found_sub[1]])/2
                    #sub_v = one_sent_embed[found_sub[1], :]
                    #sub_v = torch.mean(sub_embed, dim=0, keepdim=True)
                    object_embed = sub_v + one_sent_embed
                    for i in range(self.label_size):
                        obj_start_pred = self.activation(self.obj_start_layers[i](object_embed)).squeeze(dim=-1).cpu().numpy()
                        obj_end_pred = self.activation(self.obj_end_layers[i](object_embed)).squeeze(dim=-1).cpu().numpy()
                        found_objects = self._find_near_ht(obj_start_pred, obj_end_pred)
                        if len(found_objects) > 0:
                            found_triple[batch_index].extend(
                                [[found_sub[0], found_sub[1], obj[0], obj[1], i] for obj in found_objects])
        return {'triples': found_triple}
