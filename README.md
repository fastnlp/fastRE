# 基于fastNLP的关系抽取(Relation Extraction)模型

### 欢迎加入我们
随着知识图谱的使用越来越广泛，关系抽取作为知识图谱构建的基础，是当今最火热的研究方向之一。fastRE作为fastNLP的子项目，利用fastnlp提供的灵活而强大的基础功能，快捷便利的复现了各种经典关系抽取模型。方便大家在知识图谱项目中，开箱即用。

目前fastRE还在持续开发中，欢迎大家参与到项目中来，基于fastNLP复现各种关系抽取的经典论文和SOTA实现。可以直接pull request，我们会在第一时间审核并合并。


### 公开数据集

| Dataset | Source                                                       | Download                                                     |
| ------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| WebNLG  | [Creating Training Corpora for NLG Micro-Planners](https://www.aclweb.org/anthology/P17-1017.pdf) | [Download](https://drive.google.com/open?id=1zISxYa-8ROe2Zv8iRc82jY9QsQrfY1Vj) |
| NYT     | [Modeling relations and their mentions without labeled text](https://link.springer.com/chapter/10.1007/978-3-642-15939-8_10) | [Download](https://drive.google.com/file/d/1kAVwR051gjfKn3p6oKc7CzNT9g2Cjy6N/view) |
| DocRED  | [Docred: A large-scale document-level relation extraction dataset](https://arxiv.org/abs/1906.06127v3) | [Download](https://drive.google.com/drive/folders/1c5-0YwnoJx8NS6CV2f-NoTHR__BdkNqw?usp=sharing) |
| ...     | ...                                                          | ...                                                          |



### 已复现模型

- [ATLOP](ATLOP): [Document-Level Relation Extraction with Adaptive Thresholding and Localized Context Pooling](https://arxiv.org/pdf/2010.11304v2.pdf)
- [CasRel](CasRel): [A Novel Cascade Binary Tagging Framework for Relational Triple Extraction](https://arxiv.org/pdf/1909.03227.pdf) 

### 待实现


- SPN4RE: [Joint Entity and Relation Extraction with Set Prediction Networks](https://arxiv.org/pdf/2011.01675v2.pdf)

