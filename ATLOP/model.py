import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from fastNLP.core.const import Const as C


class ATLOPModel(nn.Module):
    def __init__(self, embed, label_size=97, dim=768, block_size=64, cls_id=0, sep_id=1):
        super(ATLOPModel, self).__init__()
        self.embed = embed
        self.dim = dim
        self.label_size = label_size
        self.block_size = block_size
        self.en_1_mlp = nn.Linear(2 * dim, dim)
        self.en_2_mlp = nn.Linear(2 * dim, dim)
        self.output_mlp = nn.Linear(dim * block_size, label_size)
        self.cls_id = [cls_id]
        self.sep_id = [sep_id]

    def process_long_sequence_bert(self, words):

        cls = torch.tensor(self.cls_id).to(words)
        sep = torch.tensor(self.sep_id).to(words)
        n, c = words.size()
        seq_len = (words > 0).sum(1).cpu().numpy().astype(np.int32).tolist()
        attention_mask = [[1.0] * cur_len + [0.0] * (c - cur_len) for cur_len in seq_len]
        attention_mask = torch.FloatTensor(attention_mask).to(words)
        if c <= 512:
            # with torch.no_grad():
            output = self.embed(
                input_ids=words, attention_mask=attention_mask, output_attentions=True
            )
            sequence_output = output[0]
            attention = output[-1][-1]
        else:
            new_input_ids, new_attention_mask, num_seg = [], [], []
            for i, l_i in enumerate(seq_len):
                if l_i <= 512:
                    new_input_ids.append(words[i, :512])
                    new_attention_mask.append(attention_mask[i, :512])
                    num_seg.append(1)
                else:
                    input_ids1 = torch.cat([words[i, :512 - 1], sep], dim=-1)
                    input_ids2 = torch.cat([cls, words[i, (l_i - 512 + 1): l_i]], dim=-1)

                    attention_mask1 = attention_mask[i, :512]
                    attention_mask2 = attention_mask[i, (l_i - 512): l_i]
                    new_input_ids.extend([input_ids1, input_ids2])
                    new_attention_mask.extend([attention_mask1, attention_mask2])
                    num_seg.append(2)
            input_ids = torch.stack(new_input_ids, dim=0)
            attention_mask = torch.stack(new_attention_mask, dim=0)
            # with torch.no_grad():
            output = self.embed(
                input_ids=input_ids,
                output_attentions=True,
            )
            sequence_output = output[0]
            attention = output[-1][-1]
            i = 0
            new_output, new_attention = [], []
            for (n_s, l_i) in zip(num_seg, seq_len):
                if n_s == 1:
                    output = F.pad(sequence_output[i], (0, 0, 0, c - 512))
                    att = F.pad(attention[i], (0, c - 512, 0, c - 512))
                    new_output.append(output)
                    new_attention.append(att)
                elif n_s == 2:
                    output1 = sequence_output[i][:512 - 1]
                    mask1 = attention_mask[i][:512 - 1]
                    att1 = attention[i][:, :512 - 1, :512 - 1]
                    output1 = F.pad(output1, (0, 0, 0, c - 512 + 1))
                    mask1 = F.pad(mask1, (0, c - 512 + 1))
                    att1 = F.pad(att1, (0, c - 512 + 1, 0, c - 512 + 1))

                    output2 = sequence_output[i + 1][1:]
                    mask2 = attention_mask[i + 1][1:]
                    att2 = attention[i + 1][:, 1:, 1:]
                    output2 = F.pad(output2, (0, 0, l_i - 512 + 1, c - l_i))
                    mask2 = F.pad(mask2, (l_i - 512 + 1, c - l_i))
                    att2 = F.pad(att2, [l_i - 512 + 1, c - l_i, l_i - 512 + 1, c - l_i])
                    mask = mask1 + mask2 + 1e-10
                    output = (output1 + output2) / mask.unsqueeze(-1)
                    att = (att1 + att2)
                    att = att / (att.sum(-1, keepdim=True) + 1e-10)
                    new_output.append(output)
                    new_attention.append(att)
                i += n_s
            sequence_output = torch.stack(new_output, dim=0)
            attention = torch.stack(new_attention, dim=0)
        return sequence_output, attention,attention_mask

    def forward(self, words, entity_star_pos, entity_pairs):
        x_emb, attention, mask = self.process_long_sequence_bert(words)

        entity_1_embed = []
        entity_2_embed = []
        context_embed = []
        for batch_index in range(len(entity_star_pos)):
            entity_embeds = []
            attention_embeds = []
            batch = entity_star_pos[batch_index]
            for pos in batch:
                entity_embed = x_emb[batch_index, pos]
                entity_embed = torch.logsumexp(entity_embed, dim=0)
                entity_embeds.append(entity_embed)

                entity_atts = attention[batch_index, :, pos]
                entity_mean_att = entity_atts.mean(1)  # 去除同一entity 多个位置
                attention_embeds.append(entity_mean_att)  # head,  att
            entity_embeds = torch.stack(entity_embeds, dim=0)
            attention_embeds = torch.stack(attention_embeds, dim=0)  # entity_size, head, att

            # 计算 两个 entity的表示

            current_entity_pair = torch.from_numpy(np.array(entity_pairs[batch_index])).to(words)
            entity_1_pos = current_entity_pair[:, 0]
            entity_2_pos = current_entity_pair[:, 1]
            entity_1 = torch.index_select(entity_embeds, 0, entity_1_pos)
            entity_2 = torch.index_select(entity_embeds, 0, entity_2_pos)
            entity_1_embed.append(entity_1)
            entity_2_embed.append(entity_2)

            # 合并两个entity 的attention。通过att，计算全文表示
            entity_att_1 = torch.index_select(attention_embeds, 0, current_entity_pair[:, 0])
            entity_att_2 = torch.index_select(attention_embeds, 0, current_entity_pair[:, 1])
            en_att = (entity_att_1 * entity_att_2).mean(1)  # 去掉HEAD, entity_len, seq_len
            en_att = en_att / (en_att.sum(dim=-1, keepdim=True) + 1e-5)  # normalize
            # x_emb[i]:seq_len,dim,  en_att:entity_len,seq_len:

            context = torch.matmul(en_att, x_emb[batch_index])

            # entity_len,dim
            context_embed.append(context)

        # (entity_size, dim)
        context_embed = torch.cat(context_embed, dim=0)
        entity_1_embed = torch.cat(entity_1_embed, dim=0)
        entity_2_embed = torch.cat(entity_2_embed, dim=0)

        left = torch.tanh(self.en_1_mlp(torch.cat([entity_1_embed, context_embed], dim=1)))
        right = torch.tanh(self.en_2_mlp(torch.cat([entity_2_embed, context_embed], dim=1)))

        b1 = left.view(-1, self.dim // self.block_size, self.block_size)
        b2 = right.view(-1, self.dim // self.block_size, self.block_size)
        bl = (b1.unsqueeze(3) * b2.unsqueeze(2)).view(-1, self.dim * self.block_size)
        logits = self.output_mlp(bl)
        output = self.predict_label(logits)
        return {C.OUTPUT: logits}

    @staticmethod
    def predict_label(logits):
        output = torch.zeros_like(logits)
        positive = logits > (logits[:, 0].unsqueeze(1))
        output[positive] = 1
        output[:, 0] = (positive.sum(1) == 0).int()
        return output
