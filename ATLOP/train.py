from datasets import load_dataset, load_json_data
from fastNLP.embeddings import BertEmbedding
from fastNLP.io import DataBundle
from fastNLP import Vocabulary
from model import ATLOPModel
from fastNLP import CrossEntropyLoss
from torch.optim import Adam
from fastNLP import AccuracyMetric
from fastNLP import Trainer
from at_loss import *
from fastNLP import LossFunc
from fastNLP.core.const import Const
import torch
from transformers import AutoConfig, AutoModel, AutoTokenizer
from acc_metric import AccMetric
from fastNLP.core.callback import  GradientClipCallback
dev_path = './data/DocRED/dev.json'
train_path = './data/DocRED/train_annotated.json'
test_path = './data/DocRED/test.json'
rel2id_path = './data/DocRED/rel2id.json'
bert_model = 'bert-base-cased'

tokenizer = AutoTokenizer.from_pretrained(bert_model)
train_raw_data = load_json_data(train_path)
dev_raw_data = load_json_data(dev_path)
test_raw_data = load_json_data(test_path)
rel2id = load_json_data(rel2id_path)

train_data = load_dataset(rel2id, train_raw_data, tokenizer)
dev_data = load_dataset(rel2id, dev_raw_data, tokenizer)
# test_data = load_dataset(rel2id, test_raw_data)

data_bundle = DataBundle(datasets={"train": train_data, "dev": dev_data})

#src_vocab = Vocabulary()
#src_vocab.from_dataset(*data_bundle.datasets.values(), field_name='words')
#src_vocab.index_dataset(*data_bundle.datasets.values(), field_name='words')
#data_bundle.set_vocab(src_vocab, 'words')

data_bundle.set_ignore_type('entity_star_pos', 'entity_pairs', 'target')
data_bundle.set_input('words', 'entity_star_pos', 'entity_pairs')
data_bundle.set_target('target') 

config = AutoConfig.from_pretrained(bert_model)
embed = AutoModel.from_pretrained(bert_model, config=config)
# embed = BertEmbedding(src_vocab, model_dir_or_name=bert_model)

model = ATLOPModel(embed, label_size=len(rel2id), cls_id=tokenizer.cls_token_id, sep_id=tokenizer.sep_token_id)

optimizer = Adam(model.parameters(), lr=3e-5,eps=1e-6)
metric = AccMetric()
device = 0 if torch.cuda.is_available() else 'cpu'  # 如果有gpu的话在gpu上运行，训练速度会更快

# optimizer = AdamW(model.parameters(), lr=3e-5,eps=1e-6)
# optimizer = AdamW(model.parameters(), lr=5e-5,momentum=0.9)
new_layer = ["en_1_mlp","en_2_mlp", "output_mlp"]
optimizer_grouped_parameters = [
    {"params": [p for n, p in model.named_parameters() if not any(nd in n for nd in new_layer)], },
    {"params": [p for n, p in model.named_parameters() if any(nd in n for nd in new_layer)], "lr": 1e-4},
]
optimizer = AdamW(optimizer_grouped_parameters, lr=5e-5,eps=1e-6)
metric = AccMetric()
loss_func = LossFunc(get_adapter_thresholding_loss, input=Const.OUTPUT, target=Const.TARGET)
total_steps = len(data_bundle.get_dataset('train')) * 30
warmup_steps = int(total_steps * 0.06)
scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=warmup_steps, num_training_steps=total_steps)

class MyCallback(Callback):
    def on_step_end(self):
        scheduler.step()
    def on_epoch_end(self):
        tester = Tester(data_bundle.get_dataset('dev'), model, metrics=F1Metric(),use_tqdm=False)
        tester.test()

callbacks = [MyCallback()]

trainer = Trainer(train_data=data_bundle.get_dataset('train'), model=model, loss=loss_func,
                  optimizer=optimizer, batch_size=2, dev_data=data_bundle.get_dataset('dev'),
                  metrics=metric, device=device,use_tqdm=False,callbacks=callbacks)
trainer.train()  # 开始训练，训练完成之后默认会加载在dev上表现最好的模型