from fastNLP.core.metrics import MetricBase
import numpy as np
from model import ATLOPModel

class AccMetric(MetricBase):
    def __init__(self):
        super().__init__()
        # 根据你的情况自定义指标
        self.corr_num = 0
        self.total = 0

    def evaluate(self, target, pred):  # 这里的名称需要和dataset中target field与model返回的key是一样的，不然找不到对应的value
        # dev或test时，每个batch结束会调用一次该方法，需要实现如何根据每个batch累加metric
        pred = ATLOPModel.predict_label(pred)
        pred = pred.cpu().numpy()
        target = np.concatenate(target, axis=0)

        false_guess = ((pred != target).sum(1)>0).sum()
        total_guess = target.shape[0]
        self.total += total_guess
        self.corr_num += total_guess - false_guess

    def get_metric(self, reset=True):  # 在这里定义如何计算metric
        acc = self.corr_num / self.total
        if reset:  # 是否清零以便重新计算
            self.corr_num = 0
            self.total = 0
        return {'acc': acc}  # 需要返回一个dict，key为该metric的名称，该名称会显示到Trainer的progress bar中

