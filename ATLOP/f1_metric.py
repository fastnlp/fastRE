from fastNLP.core.metrics import MetricBase
import numpy as np
from model import ATLOPModel

class F1Metric(MetricBase):
    def __init__(self):
        super().__init__()
        self.true_positive = 0
        self.real_positive = 0
        self.pred_positive = 0

    def evaluate(self, target, pred):  # 这里的名称需要和dataset中target field与model返回的key是一样的，不然找不到对应的value
        # dev或test时，每个batch结束会调用一次该方法，需要实现如何根据每个batch累加metric
        pred = ATLOPModel.predict_label(pred)
        pred = pred.cpu().numpy()
        target = np.concatenate(target, axis=0)

        #过滤掉UNKOW预测
        pred[:, 0] = 0
        target[:, 0] = 0
        self.pred_positive = self.pred_positive + pred.sum()
        compare = pred*target
        self.true_positive = self.true_positive + compare.sum()

        self.real_positive = self.real_positive + target.sum()



    def get_metric(self, reset=True):  # 在这里定义如何计算metric
        precise = self.true_positive / self.pred_positive
        recall = self.true_positive/ self.real_positive
        f1 = 2 * precise * recall/(precise+recall)
        if reset:  # 是否清零以便重新计算
            self.true_positive = 0
            self.pred_positive = 0
            self.real_positive = 0

        return {'f1': f1}  # 需要返回一个dict，key为该metric的名称，该名称会显示到Trainer的progress bar中

