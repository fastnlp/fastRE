import numpy as np
import torch
from torch.functional import F


def get_adapter_thresholding_loss(input, target):
    # target = torch.from_numpy(np.concatenate(target, axis=0)).to(input)
    # l2_pos_loss = torch.sum(-F.log_softmax(input, dim=-1) * target, dim=-1)
    # return l2_pos_loss.sum()

    # input (en_pair, num_label)
    # target:(batch, en_pair, multihot)
    target = torch.from_numpy(np.concatenate(target, axis=0)).to(input)

    # 包含正例的样本
    # L1 loss
    pos_sample = target[:, 0] == 0
    positive_mask = target[pos_sample]
    l1_label = target[pos_sample]
    positive_mask[:, 0] = 1
    l1_output = input[pos_sample] + (1 - positive_mask) * -1e9
    l1_pos_loss = torch.sum(-F.log_softmax(l1_output, dim=-1) * l1_label, dim=-1)

    # l2 loss pos
    negative_mask = target[pos_sample]
    negative_mask = (negative_mask == 0).type(torch.FloatTensor).to(input)
    l2_label = torch.zeros_like(negative_mask, dtype=torch.float).to(input)
    l2_label[:, 0] = 1
    l2_output = input[pos_sample] + (1 - negative_mask) * -1e9
    l2_pos_loss = torch.sum(-F.log_softmax(l2_output, dim=-1) * l2_label, dim=-1)

    # 只有负例的样本
    # l2 loss neg
    neg_sample = target[:, 0] == 1
    l2_label = target[neg_sample]
    l2_neg_loss = torch.sum(-F.log_softmax(input[neg_sample], dim=-1) * l2_label, dim=-1)

    loss = torch.cat([l1_pos_loss, l2_pos_loss, l2_neg_loss]).mean()
    return loss
