## Document-Level Relation Extraction with Adaptive Thresholding and Localized Context Pooling

### 论文

https://arxiv.org/pdf/2010.11304v2.pdf

### 使用数据集：

[DocRED](https://github.com/thunlp/DocRED/tree/master/data)

### 训练过程：

1. 安装fastnlp和huggingface transformers
2. 下载数据集，解压缩到./data/DocRED目录
3. 使用python train.py 或者train.ipynb 进行训练

### 结果：

论文中，BERT-ATLOP模型的f1值为61.09

我们的复现结果 f1值 53.7 。

没有使用论文中提到的apex混合精度训练，这可能是不如论文预期的原因。



关于bert超出512个字符的处理，引用了ATLOP官方实现的代码。

https://github.com/wzhouad/ATLOP/blob/main/long_seq.py





