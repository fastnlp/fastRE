from fastNLP.io.pipe import Pipe
from fastNLP.io import DataBundle
from fastNLP import DataSet
from fastNLP import Instance
import numpy as np
import json


def load_json_data(path):
    with open(path, 'r') as f:
        data = json.load(f)
    return data;


def load_dataset(rel2id, raw_dataset, tokenizer):
    cls = '[CLS]'
    sep = '[SEP]'
    Na = 'Na'
    dataset = DataSet()
    for doc in raw_dataset:
        # 1.sentence拼接
        # 2.插入*
        # 3.tokenizer
        # 4.正负样本生成
        wholedoc = []
        wholedoc.append(cls)
        sent_entity_start = {}
        sent_entity_end = {}
        new_entity_star_pos = []  # 记录每个mention的entity星号  在tokenzior, 插入特殊字符后的位置

        for en_id, entities in enumerate(doc['vertexSet']):
            for entity in entities:
                start_pos = (entity['sent_id'], entity['pos'][0])
                end_pos = (entity['sent_id'], entity['pos'][1] - 1)
                if start_pos not in sent_entity_start:
                    sent_entity_start[start_pos] = []
                if end_pos not in sent_entity_end:
                    sent_entity_end[end_pos] = []
                sent_entity_start[start_pos].append(en_id)
                sent_entity_end[end_pos].append(en_id)
            new_entity_star_pos.append([])

        for sent_id, sent in enumerate(doc['sents']):
            for key_id, key in enumerate(sent):
                tokens = tokenizer.tokenize(key)
                # tokens = key
                if (sent_id, key_id) in sent_entity_start:
                    en_ids = sent_entity_start[(sent_id, key_id)]
                    for en_id in en_ids:
                        new_entity_star_pos[en_id].append(len(wholedoc))  # entity * 标志的新位置
                    wholedoc.extend(['*'] + tokens)
                elif (sent_id, key_id) in sent_entity_end:
                    wholedoc.extend(tokens + ['*'])
                else:
                    wholedoc.extend(tokens)

        wholedoc.append(sep)
        wholedoc_ids = tokenizer.convert_tokens_to_ids(wholedoc)

        pos_pair = {}
        for label in doc['labels']:
            pair = (label['h'], label['t'])
            if pair not in pos_pair:
                pos_pair[pair] = []
            pos_pair[pair].append(label['r'])

        entity_pair = []
        relations = []
        for entity_1 in range(len(doc['vertexSet'])):
            for entity_2 in range(len(doc['vertexSet'])):
                if entity_1 != entity_2:
                    pair = (entity_1, entity_2)
                    entity_pair.append([entity_1, entity_2])
                    if pair in pos_pair:
                        multi_hot = np.zeros(len(rel2id))
                        for re in pos_pair[pair]:
                            temp = np.zeros(len(rel2id))
                            temp[rel2id[re]] = 1
                            multi_hot += temp
                        relations.append(multi_hot)
                    else:
                        multi_hot = np.zeros(len(rel2id))
                        multi_hot[rel2id[Na]] = 1
                        relations.append(multi_hot)
        dataset.append(Instance(words=wholedoc_ids, entity_star_pos=new_entity_star_pos,
                                entity_pairs=entity_pair, target=relations))
    return dataset
